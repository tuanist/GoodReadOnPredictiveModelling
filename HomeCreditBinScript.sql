/****** Script for SelectTopNRows command from SSMS  ******/

select t.SK_ID_CURR,
		null as TARGET,
		EXT_SOURCE_3 ,
	  EXT_SOURCE_2 ,
	  EXT_SOURCE_1 ,
	  DAYS_EMPLOYED ,
	  DAYS_BIRTH ,
	  ORGANIZATION_TYPE ,
	  AMT_GOODS_PRICE ,
	  REGION_RATING_CLIENT ,
	  AMT_CREDIT ,
	  NAME_EDUCATION_TYPE ,
	  REGION_POPULATION_RELATIVE ,
	  DAYS_LAST_PHONE_CHANGE ,
	  AMT_ANNUITY ,
	  FLOORSMAX_MEDI ,
	  NAME_FAMILY_STATUS ,
	  case when FLAG_DOCUMENT_13 = 1 then 'G1'
			when FLAG_DOCUMENT_15 = 1 then 'G1'
			when FLAG_DOCUMENT_14 = 1 then 'G2'
			when FLAG_DOCUMENT_16 = 1 then 'G3'
			when FLAG_DOCUMENT_6 = 1 then 'G4'
			when FLAG_DOCUMENT_18 = 1 then 'G4'
			when FLAG_DOCUMENT_11 = 1 then 'G5'
			when FLAG_DOCUMENT_9 = 1 then 'G5'
			when FLAG_DOCUMENT_8 = 1 then 'G6'
			when FLAG_DOCUMENT_3 = 1 then 'G7'
			end DOCUMENT_GROUP,
	  FLAG_OWN_CAR ,
	  WEEKDAY_APPR_PROCESS_START,
	  DEF_30_CNT_SOCIAL_CIRCLE,
	  OCCUPATION_TYPE 
into [HomeCredit].[dbo].application_combined
from [HomeCredit].[dbo].application_test1 t
union all

select t.SK_ID_CURR,
TARGET,
EXT_SOURCE_3 ,
EXT_SOURCE_2 ,
EXT_SOURCE_1 ,
DAYS_EMPLOYED ,
DAYS_BIRTH ,
ORGANIZATION_TYPE ,
AMT_GOODS_PRICE ,
REGION_RATING_CLIENT ,
AMT_CREDIT ,
NAME_EDUCATION_TYPE ,
REGION_POPULATION_RELATIVE ,
DAYS_LAST_PHONE_CHANGE ,
AMT_ANNUITY ,
FLOORSMAX_MEDI ,
NAME_FAMILY_STATUS ,
case when FLAG_DOCUMENT_13 = 1 then 'G1'
	when FLAG_DOCUMENT_15 = 1 then 'G1'
	when FLAG_DOCUMENT_14 = 1 then 'G2'
	when FLAG_DOCUMENT_16 = 1 then 'G3'
	when FLAG_DOCUMENT_6 = 1 then 'G4'
	when FLAG_DOCUMENT_18 = 1 then 'G4'
	when FLAG_DOCUMENT_11 = 1 then 'G5'
	when FLAG_DOCUMENT_9 = 1 then 'G5'
	when FLAG_DOCUMENT_8 = 1 then 'G6'
	when FLAG_DOCUMENT_3 = 1 then 'G7'
	end DOCUMENT_GROUP,
FLAG_OWN_CAR ,
WEEKDAY_APPR_PROCESS_START,
DEF_30_CNT_SOCIAL_CIRCLE,
OCCUPATION_TYPE 
from [HomeCredit].[dbo].application_train1 t;

/****** Script for SelectTopNRows command from SSMS  ******/
SELECT cast(SK_ID_PREV as real) as SK_ID_PREV
      ,cast(SK_ID_CURR as real) SK_ID_CURR
      ,NAME_CONTRACT_TYPE
      ,cast(AMT_ANNUITY as real) AMT_ANNUITY
      ,cast(AMT_APPLICATION as real) AMT_APPLICATION
      ,cast(AMT_CREDIT as real) AMT_CREDIT
      ,cast(AMT_DOWN_PAYMENT as real) AMT_DOWN_PAYMENT
      ,cast(AMT_GOODS_PRICE as real) AMT_GOODS_PRICE
      ,WEEKDAY_APPR_PROCESS_START
      ,cast(HOUR_APPR_PROCESS_START as real) HOUR_APPR_PROCESS_START
      ,FLAG_LAST_APPL_PER_CONTRACT
      ,cast(NFLAG_LAST_APPL_IN_DAY as real) NFLAG_LAST_APPL_IN_DAY
      ,cast(RATE_DOWN_PAYMENT as real) RATE_DOWN_PAYMENT
      ,cast(RATE_INTEREST_PRIMARY as real) RATE_INTEREST_PRIMARY
      ,cast(RATE_INTEREST_PRIVILEGED as real) RATE_INTEREST_PRIVILEGED
      ,NAME_CASH_LOAN_PURPOSE
      ,NAME_CONTRACT_STATUS
      ,cast(DAYS_DECISION as real) DAYS_DECISION
      ,NAME_PAYMENT_TYPE
      ,CODE_REJECT_REASON
      ,NAME_TYPE_SUITE
      ,NAME_CLIENT_TYPE
      ,NAME_GOODS_CATEGORY
      ,NAME_PORTFOLIO
      ,NAME_PRODUCT_TYPE
      ,CHANNEL_TYPE
      ,cast(SELLERPLACE_AREA as real) SELLERPLACE_AREA
      ,NAME_SELLER_INDUSTRY
      ,cast(CNT_PAYMENT as real) CNT_PAYMENT
      ,NAME_YIELD_GROUP
      ,PRODUCT_COMBINATION
      ,cast(DAYS_FIRST_DRAWING as real) DAYS_FIRST_DRAWING
      ,cast(DAYS_FIRST_DUE as real) DAYS_FIRST_DUE
      ,cast(DAYS_LAST_DUE_1ST_VERSION as real) DAYS_LAST_DUE_1ST_VERSION
      ,cast(DAYS_LAST_DUE as real) DAYS_LAST_DUE
      ,cast(DAYS_TERMINATION as real) DAYS_TERMINATION
      ,NFLAG_INSURED_ON_APPROVAL NFLAG_INSURED_ON_APPROVAL
into HomeCredit.dbo.previous_application1
  FROM HomeCredit.dbo.previous_application;
  

SELECT SK_ID_CURR
      ,count(distinct (case when NAME_CONTRACT_STATUS = 'Approved' then NAME_CONTRACT_TYPE end)) as NAME_CONTRACT_TYPE_APR_CNT
      ,count(distinct NAME_CONTRACT_TYPE ) as NAME_CONTRACT_TYPE_CNT
      
      ,max(case when NAME_CONTRACT_STATUS = 'Approved' then AMT_ANNUITY else 0 end) as AMT_ANNUITY_APR_MAX
      ,sum(case when NAME_CONTRACT_STATUS = 'Approved' then AMT_ANNUITY else 0 end) as AMT_ANNUITY_APR_SUM
      ,sum(AMT_ANNUITY) as AMT_ANNUITY_SUM
      
      ,sum(case when NAME_CONTRACT_STATUS = 'Approved' then AMT_APPLICATION else 0 end) as AMT_APPLICATION_APR_SUM
      ,sum(AMT_APPLICATION) as AMT_APPLICATION_SUM
      
      ,sum(case when NAME_CONTRACT_STATUS = 'Approved' then AMT_CREDIT else 0 end) as AMT_CREDIT_APR_SUM
      ,sum(AMT_CREDIT) as AMT_CREDIT_SUM


      ,max(AMT_DOWN_PAYMENT) AMT_DOWN_PAYMENT_MAX
      ,sum(AMT_DOWN_PAYMENT) AMT_DOWN_PAYMENT_SUM
      
      ,max(AMT_GOODS_PRICE) AMT_GOODS_PRICE_MAX
      ,sum(AMT_GOODS_PRICE) AMT_GOODS_PRICE_SUM
		
      ,max(RATE_DOWN_PAYMENT) RATE_DOWN_PAYMENT_MAX
      ,avg(RATE_DOWN_PAYMENT) RATE_DOWN_PAYMENT_AVG
      
      ,count(NAME_CONTRACT_STATUS) NAME_CONTRACT_STATUS_CNT
      ,sum(case when NAME_CONTRACT_STATUS = 'Approved' then 1 else 0 end) NAME_CONTRACT_STATUS_APR_CNT
      ,sum(case when NAME_CONTRACT_STATUS = 'Refused' then 1 else 0 end) NAME_CONTRACT_STATUS_REF_CNT
      ,sum(case when NAME_CONTRACT_STATUS = 'Approved' then 1 else 0 end)/count(NAME_CONTRACT_STATUS) APPROVED_RATE
      
      ,max(- DAYS_DECISION) DAYS_DECISION_MAX
      ,min(- DAYS_DECISION) DAYS_DECISION_MIN
      
      ,MAX(SELLERPLACE_AREA) SELLERPLACE_AREA_MAX
      ,min(SELLERPLACE_AREA) SELLERPLACE_AREA_MIN
      ,avg(SELLERPLACE_AREA) SELLERPLACE_AREA_AVG
      
      ,sum(CNT_PAYMENT) CNT_PAYMENT_SUM
      ,max(CNT_PAYMENT) CNT_PAYMENT_MAX
      ,min(CNT_PAYMENT) CNT_PAYMENT_MIN
      
      ,sum(case when (DAYS_LAST_DUE - DAYS_FIRST_DUE)/30 <= CNT_PAYMENT 
					 and CNT_PAYMENT <> 0 and DAYS_FIRST_DUE <> 0 then 1 else 0 end) NAME_CONTRACT_STATUS_CLS_CNT

      ,min(case when DAYS_FIRST_DRAWING = 365243 then 0 else -DAYS_FIRST_DRAWING end) DAYS_FIRST_DRAWING_LAST
      ,max(case when DAYS_FIRST_DRAWING = 365243 then 0 else -DAYS_FIRST_DRAWING end) DAYS_FIRST_DRAWING_FIRST
      ,min(case when DAYS_FIRST_DUE = 365243 then 0 else -DAYS_FIRST_DUE end) DAYS_FIRST_DUE_LAST
      ,max(case when DAYS_FIRST_DUE = 365243 then 0 else -DAYS_FIRST_DUE end) DAYS_FIRST_DUE_FIRST
      
      ,min(case when DAYS_LAST_DUE_1ST_VERSION = 365243 then 0 else -DAYS_LAST_DUE_1ST_VERSION end) DAYS_LAST_DUE_1ST_VERSION_LAST
      ,max(case when DAYS_LAST_DUE_1ST_VERSION = 365243 then 0 else -DAYS_LAST_DUE_1ST_VERSION end) DAYS_LAST_DUE_1ST_VERSION_FIRST

      ,min(case when DAYS_LAST_DUE = 365243 then 0 else -DAYS_LAST_DUE end) DAYS_LAST_DUE_LAST
      ,max(case when DAYS_LAST_DUE = 365243 then 0 else -DAYS_LAST_DUE end) DAYS_LAST_DUE_FIRST
      ,min(case when DAYS_TERMINATION = 365243 then 0 else -DAYS_TERMINATION end) DAYS_TERMINATION_LAST
      ,max(case when DAYS_TERMINATION = 365243 then 0 else -DAYS_TERMINATION end) DAYS_TERMINATION_FIRST
into HomeCredit.dbo.previous_application_data1
FROM HomeCredit.dbo.previous_application1
group by SK_ID_CURR;



select a.SK_ID_CURR
	   ,'G' + cast(NTILE(20) OVER(ORDER BY NAME_CONTRACT_TYPE_APR_CNT)as VARCHAR(10)) AS NAME_CONTRACT_TYPE_APR_CNT
       ,'G' + cast(NTILE(20) OVER(ORDER BY NAME_CONTRACT_TYPE_CNT)as VARCHAR(10)) AS NAME_CONTRACT_TYPE_CNT
       ,'G' + cast(NTILE(20) OVER(ORDER BY AMT_ANNUITY_APR_MAX)as VARCHAR(10)) AS AMT_ANNUITY_APR_MAX
       ,'G' + cast(NTILE(20) OVER(ORDER BY AMT_ANNUITY_APR_SUM)as VARCHAR(10)) AS AMT_ANNUITY_APR_SUM
       ,'G' + cast(NTILE(20) OVER(ORDER BY AMT_ANNUITY_SUM)as VARCHAR(10)) AS AMT_ANNUITY_SUM
       ,'G' + cast(NTILE(20) OVER(ORDER BY AMT_APPLICATION_SUM)as VARCHAR(10)) AS AMT_APPLICATION_SUM
		
		,'G' + cast(NTILE(20) OVER(ORDER BY AMT_CREDIT_APR_SUM) as VARCHAR(10)) AS AMT_CREDIT_APR_SUM
		,'G' + cast(NTILE(20) OVER(ORDER BY AMT_CREDIT_SUM) as VARCHAR(10)) AS AMT_CREDIT_SUM
		,'G' + cast(NTILE(20) OVER(ORDER BY AMT_DOWN_PAYMENT_MAX) as VARCHAR(10)) AS AMT_DOWN_PAYMENT_MAX
		,'G' + cast(NTILE(20) OVER(ORDER BY AMT_DOWN_PAYMENT_SUM) as VARCHAR(10)) AS AMT_DOWN_PAYMENT_SUM
		,'G' + cast(NTILE(20) OVER(ORDER BY AMT_GOODS_PRICE_MAX) as VARCHAR(10)) AS AMT_GOODS_PRICE_MAX
		,'G' + cast(NTILE(20) OVER(ORDER BY AMT_GOODS_PRICE_SUM) as VARCHAR(10)) AS AMT_GOODS_PRICE_SUM
		,'G' + cast(NTILE(20) OVER(ORDER BY RATE_DOWN_PAYMENT_MAX) as VARCHAR(10)) AS RATE_DOWN_PAYMENT_MAX
		,'G' + cast(NTILE(20) OVER(ORDER BY RATE_DOWN_PAYMENT_AVG) as VARCHAR(10)) AS RATE_DOWN_PAYMENT_AVG
		,'G' + cast(NTILE(20) OVER(ORDER BY NAME_CONTRACT_STATUS_CNT) as VARCHAR(10)) AS NAME_CONTRACT_STATUS_CNT
		,'G' + cast(NTILE(20) OVER(ORDER BY NAME_CONTRACT_STATUS_APR_CNT) as VARCHAR(10)) AS NAME_CONTRACT_STATUS_APR_CNT
		,'G' + cast(NTILE(20) OVER(ORDER BY NAME_CONTRACT_STATUS_REF_CNT) as VARCHAR(10)) AS NAME_CONTRACT_STATUS_REF_CNT
		,'G' + cast(NTILE(20) OVER(ORDER BY APPROVED_RATE) as VARCHAR(10)) AS APPROVED_RATE
		,'G' + cast(NTILE(20) OVER(ORDER BY DAYS_DECISION_MAX) as VARCHAR(10)) AS DAYS_DECISION_MAX
		,'G' + cast(NTILE(20) OVER(ORDER BY DAYS_DECISION_MIN) as VARCHAR(10)) AS DAYS_DECISION_MIN
		,'G' + cast(NTILE(20) OVER(ORDER BY SELLERPLACE_AREA_MAX) as VARCHAR(10)) AS SELLERPLACE_AREA_MAX
		,'G' + cast(NTILE(20) OVER(ORDER BY SELLERPLACE_AREA_MIN) as VARCHAR(10)) AS SELLERPLACE_AREA_MIN
		,'G' + cast(NTILE(20) OVER(ORDER BY SELLERPLACE_AREA_AVG) as VARCHAR(10)) AS SELLERPLACE_AREA_AVG
		,'G' + cast(NTILE(20) OVER(ORDER BY CNT_PAYMENT_SUM) as VARCHAR(10)) AS CNT_PAYMENT_SUM
		,'G' + cast(NTILE(20) OVER(ORDER BY CNT_PAYMENT_MAX) as VARCHAR(10)) AS CNT_PAYMENT_MAX
		,'G' + cast(NTILE(20) OVER(ORDER BY CNT_PAYMENT_MIN) as VARCHAR(10)) AS CNT_PAYMENT_MIN
		,'G' + cast(NTILE(20) OVER(ORDER BY NAME_CONTRACT_STATUS_CLS_CNT) as VARCHAR(10)) AS NAME_CONTRACT_STATUS_CLS_CNT
		,'G' + cast(NTILE(20) OVER(ORDER BY DAYS_FIRST_DRAWING_LAST) as VARCHAR(10)) AS DAYS_FIRST_DRAWING_LAST
		,'G' + cast(NTILE(20) OVER(ORDER BY DAYS_FIRST_DRAWING_FIRST) as VARCHAR(10)) AS DAYS_FIRST_DRAWING_FIRST
		,'G' + cast(NTILE(20) OVER(ORDER BY DAYS_FIRST_DUE_LAST) as VARCHAR(10)) AS DAYS_FIRST_DUE_LAST
		,'G' + cast(NTILE(20) OVER(ORDER BY DAYS_FIRST_DUE_FIRST) as VARCHAR(10)) AS DAYS_FIRST_DUE_FIRST
		,'G' + cast(NTILE(20) OVER(ORDER BY DAYS_LAST_DUE_1ST_VERSION_LAST) as VARCHAR(10)) AS DAYS_LAST_DUE_1ST_VERSION_LAST
		,'G' + cast(NTILE(20) OVER(ORDER BY DAYS_LAST_DUE_1ST_VERSION_FIRST) as VARCHAR(10)) AS DAYS_LAST_DUE_1ST_VERSION_FIRST
		,'G' + cast(NTILE(20) OVER(ORDER BY DAYS_LAST_DUE_LAST) as VARCHAR(10)) AS DAYS_LAST_DUE_LAST
		,'G' + cast(NTILE(20) OVER(ORDER BY DAYS_LAST_DUE_FIRST) as VARCHAR(10)) AS DAYS_LAST_DUE_FIRST
		,'G' + cast(NTILE(20) OVER(ORDER BY DAYS_TERMINATION_LAST) as VARCHAR(10)) AS DAYS_TERMINATION_LAST
		,'G' + cast(NTILE(20) OVER(ORDER BY DAYS_TERMINATION_FIRST) as VARCHAR(10)) AS DAYS_TERMINATION_FIRST
into HomeCredit.dbo.previous_application_bin
From HomeCredit.dbo.previous_application_data1 a;


select SK_ID_CURR, SK_ID_PREV, NUM_INSTALMENT_NUMBER, NUM_INSTALMENT_VERSION,DAYS_INSTALMENT, t.AMT_INSTALMENT,
		MAX(DAYS_ENTRY_PAYMENT) DAYS_ENTRY_PAYMENT,
		MAX( DAYS_ENTRY_PAYMENT - DAYS_INSTALMENT) DPD,
		isnull(SUM(AMT_PAYMENT),0) AMT_PAYMENT,
		SUM(case when DAYS_ENTRY_PAYMENT > DAYS_INSTALMENT then AMT_PAYMENT else 0 end) AMT_PAYMENT_LATE,
		SUM(case when DAYS_ENTRY_PAYMENT < DAYS_INSTALMENT then AMT_PAYMENT else 0 end) AMT_PAYMENT_EARLY
into [HomeCredit].[dbo].[installments_payments_month]
from [HomeCredit].[dbo].[installments_payments] t
where isnull(t.AMT_PAYMENT,0) = 0 or isnull(t.AMT_PAYMENT,0) >= isnull(t.AMT_INSTALMENT)*0.05
group  by SK_ID_CURR, SK_ID_PREV, NUM_INSTALMENT_NUMBER, NUM_INSTALMENT_VERSION, DAYS_INSTALMENT, t.AMT_INSTALMENT
;

select t.SK_ID_CURR,
		max(case when t.SK_ID_CURR IS not null then 1 else 0 end) as INST_FLAG,
		min(t.NUM_INSTALMENT_VERSION) NUM_INSTALMENT_VERSION,
		MAX(t.DPD) DPD_MAX_INST,
		MAX(case when -DAYS_INSTALMENT/30 <= 3 then t.DPD else 0 end) as DPD_MAX_3M_INST,
		MAX(case when -DAYS_INSTALMENT/30 <= 6 then t.DPD else 0 end) as DPD_MAX_6M_INST,
		MAX(case when -DAYS_INSTALMENT/30 <= 12 then t.DPD else 0 end) as DPD_MAX_12M_INST,
		MAX(case when -DAYS_INSTALMENT/30 <= 18 then t.DPD else 0 end) as DPD_MAX_18M_INST,
		MAX(case when -DAYS_INSTALMENT/30 <= 24 then t.DPD else 0 end) as DPD_MAX_24M_INST,
		MAX(case when -DAYS_INSTALMENT/30 <= 48 then t.DPD else 0 end) as DPD_MAX_48M_INST,
		MAX(case when -DAYS_INSTALMENT/30 <= 72 then t.DPD else 0 end) as DPD_MAX_72M_INST,
		
		max(case when t.NUM_INSTALMENT_NUMBER =1 then -DAYS_INSTALMENT/30 end) as ACC_AGE_OLDEST_INST,
		min(case when t.NUM_INSTALMENT_NUMBER =1 then -DAYS_INSTALMENT/30 end) as ACC_AGE_NEWEST_INST,
		
		sum(case when t.DPD >= 1 then 1 else 0 end) as TIMES_DPD1_INST,
		sum(case when t.DPD >= 2 then 1 else 0 end) as TIMES_DPD2_INST,
		sum(case when t.DPD >= 3 then 1 else 0 end) as TIMES_DPD3_INST,
		sum(case when t.DPD >= 4 then 1 else 0 end) as TIMES_DPD4_INST,
		sum(case when t.DPD >= 5 then 1 else 0 end) as TIMES_DPD5_INST,
		sum(case when t.DPD >= 10 then 1 else 0 end) as TIMES_DPD10_INST,
		sum(case when t.DPD >= 15 then 1 else 0 end) as TIMES_DPD15_INST,
		sum(case when t.DPD >= 20 then 1 else 0 end) as TIMES_DPD20_INST,

		sum(case when t.DPD >= 1 and -DAYS_INSTALMENT/30 <= 24 then 1 else 0 end) as TIMES_DPD1_24M_INST,


		count(distinct (case when t.DPD >=5 then t.SK_ID_PREV end)) as ACC_DPD5_INST,
		count(distinct (case when t.DPD >=10 then t.SK_ID_PREV end)) as ACC_DPD10_INST,
		count(distinct (case when t.DPD >=15 then t.SK_ID_PREV end)) as ACC_DPD15_INST,
		count(distinct (case when t.DPD >=20 then t.SK_ID_PREV end)) as ACC_DPD20_INST,
		
		count(distinct t.SK_ID_PREV) - count(distinct (case when t.DPD >=5 then t.SK_ID_PREV end)) as ACC_NVER_DPD5_INST,
		count(distinct t.SK_ID_PREV) - count(distinct (case when t.DPD >=10 then t.SK_ID_PREV end)) as ACC_NVER_DPD10_INST,
		count(distinct t.SK_ID_PREV) - count(distinct (case when t.DPD >=15 then t.SK_ID_PREV end)) as ACC_NVER_DPD15_INST,
		count(distinct t.SK_ID_PREV) - count(distinct (case when t.DPD >=20 then t.SK_ID_PREV end)) as ACC_NVER_DPD20_INST,
		

		count(distinct (case when t.DPD >=5 and -DAYS_INSTALMENT/30 <3 then t.SK_ID_PREV end)) as ACC_DPD5_3M_INST,
		count(distinct (case when t.DPD >=5 and -DAYS_INSTALMENT/30 <6 then t.SK_ID_PREV end)) as ACC_DPD5_6M_INST,
		count(distinct (case when t.DPD >=5 and -DAYS_INSTALMENT/30 <12 then t.SK_ID_PREV end)) as ACC_DPD5_12M_INST,
		count(distinct (case when t.DPD >=5 and -DAYS_INSTALMENT/30 <18 then t.SK_ID_PREV end)) as ACC_DPD5_18M_INST,
		count(distinct (case when t.DPD >=5 and -DAYS_INSTALMENT/30 <24 then t.SK_ID_PREV end)) as ACC_DPD5_24M_INST,
		count(distinct (case when t.DPD >=5 and -DAYS_INSTALMENT/30 <48 then t.SK_ID_PREV end)) as ACC_DPD5_48M_INST,
		count(distinct (case when t.DPD >=5 and -DAYS_INSTALMENT/30 <72 then t.SK_ID_PREV end)) as ACC_DPD5_72M_INST,
		
		count(distinct (case when t.DPD >=10 and -DAYS_INSTALMENT/30 <3 then t.SK_ID_PREV end)) as ACC_DPD10_3M_INST,
		count(distinct (case when t.DPD >=10 and -DAYS_INSTALMENT/30 <6 then t.SK_ID_PREV end)) as ACC_DPD10_6M_INST,
		count(distinct (case when t.DPD >=10 and -DAYS_INSTALMENT/30 <12 then t.SK_ID_PREV end)) as ACC_DPD10_12M_INST,
		count(distinct (case when t.DPD >=10 and -DAYS_INSTALMENT/30 <18 then t.SK_ID_PREV end)) as ACC_DPD10_18M_INST,
		count(distinct (case when t.DPD >=10 and -DAYS_INSTALMENT/30 <24 then t.SK_ID_PREV end)) as ACC_DPD10_24M_INST,
		count(distinct (case when t.DPD >=10 and -DAYS_INSTALMENT/30 <48 then t.SK_ID_PREV end)) as ACC_DPD10_48M_INST,
		count(distinct (case when t.DPD >=10 and -DAYS_INSTALMENT/30 <72 then t.SK_ID_PREV end)) as ACC_DPD10_72M_INST,
		
		count(distinct (case when t.DPD >=15 and -DAYS_INSTALMENT/30 <6 then t.SK_ID_PREV end)) as ACC_DPD15_6M_INST,
		count(distinct (case when t.DPD >=15 and -DAYS_INSTALMENT/30 <12 then t.SK_ID_PREV end)) as ACC_DPD15_12M_INST,
		count(distinct (case when t.DPD >=15 and -DAYS_INSTALMENT/30 <18 then t.SK_ID_PREV end)) as ACC_DPD15_18M_INST,
		count(distinct (case when t.DPD >=15 and -DAYS_INSTALMENT/30 <24 then t.SK_ID_PREV end)) as ACC_DPD15_24M_INST,
		count(distinct (case when t.DPD >=15 and -DAYS_INSTALMENT/30 <48 then t.SK_ID_PREV end)) as ACC_DPD15_48M_INST,
		count(distinct (case when t.DPD >=15 and -DAYS_INSTALMENT/30 <72 then t.SK_ID_PREV end)) as ACC_DPD15_72M_INST,
	
		count(distinct (case when t.DPD >= 5 and t.DAYS_ENTRY_PAYMENT = 0 then t.SK_ID_PREV end)) as ACC_CURR_DPD5_INST,
		count(distinct (case when t.DPD >= 10 and t.DAYS_ENTRY_PAYMENT = 0 then t.SK_ID_PREV end)) as ACC_CURR_DPD10_INST,
		count(distinct (case when t.DPD >= 15 and t.DAYS_ENTRY_PAYMENT = 0 then t.SK_ID_PREV end)) as ACC_CURR_DPD15_INST,
		count(distinct (case when t.DPD >= 20 and t.DAYS_ENTRY_PAYMENT = 0 then t.SK_ID_PREV end)) as ACC_CURR_DPD20_INST,
		count(distinct (case when t.DPD >= 25 and t.DAYS_ENTRY_PAYMENT = 0 then t.SK_ID_PREV end)) as ACC_CURR_DPD25_INST,
		
		sum((case when t.DPD >= 5 and t.DAYS_ENTRY_PAYMENT = 0 then t.AMT_INSTALMENT else 0 end)) as AMT_CURR_DPD5_INST,
		sum((case when t.DPD >= 10 and t.DAYS_ENTRY_PAYMENT = 0 then t.AMT_INSTALMENT else 0 end)) as AMT_CURR_DPD10_INST,
		sum((case when t.DPD >= 15 and t.DAYS_ENTRY_PAYMENT = 0 then t.AMT_INSTALMENT else 0 end)) as AMT_CURR_DPD15_INST,
		sum((case when t.DPD >= 20 and t.DAYS_ENTRY_PAYMENT = 0 then t.AMT_INSTALMENT else 0 end)) as AMT_CURR_DPD20_INST,
		sum((case when t.DPD >= 25 and t.DAYS_ENTRY_PAYMENT = 0 then t.AMT_INSTALMENT else 0 end)) as AMT_CURR_DPD25_INST,

		sum(AMT_PAYMENT_LATE) AMT_PAYMENT_LATE,
		sum(AMT_PAYMENT_EARLY) AMT_PAYMENT_EARLY,
		case when sum(AMT_INSTALMENT) > 0 then sum(AMT_PAYMENT_LATE) /sum(AMT_INSTALMENT) end AMT_PAYMENT_LATE_PCT,
		case when sum(AMT_INSTALMENT) > 0 then sum(AMT_PAYMENT_EARLY)/sum(AMT_INSTALMENT) end AMT_PAYMENT_EARLY_PCT

		sum(case when -DAYS_INSTALMENT/30 <3 and AMT_INSTALMENT > 0 then AMT_PAYMENT_LATE end)/
			sum(case when -DAYS_INSTALMENT/30 <3 and AMT_INSTALMENT > 0 then AMT_INSTALMENT end)  AMT_PAYMENT_LATE_3M_PCT,
		sum(case when -DAYS_INSTALMENT/30 <6 and AMT_INSTALMENT > 0 then AMT_PAYMENT_LATE end ) /
			sum(case when -DAYS_INSTALMENT/30 <6 and AMT_INSTALMENT > 0 then AMT_INSTALMENT end)   AMT_PAYMENT_LATE_6M_PCT,
		sum(case when -DAYS_INSTALMENT/30 <12 and AMT_INSTALMENT > 0 then AMT_PAYMENT_LATE end ) /
			sum(case when -DAYS_INSTALMENT/30 <12 and AMT_INSTALMENT > 0 then AMT_INSTALMENT end)  AMT_PAYMENT_LATE_12M_PCT,
		sum(case when -DAYS_INSTALMENT/30 <18 and AMT_INSTALMENT > 0 then AMT_PAYMENT_LATE end ) /
			sum(case when -DAYS_INSTALMENT/30 <18 and AMT_INSTALMENT > 0 then AMT_INSTALMENT end) AMT_PAYMENT_LATE_18M_PCT,
		sum(case when -DAYS_INSTALMENT/30 <24 and AMT_INSTALMENT > 0 then AMT_PAYMENT_LATE end ) /
			sum(case when -DAYS_INSTALMENT/30 <24 and AMT_INSTALMENT > 0 then AMT_INSTALMENT end) AMT_PAYMENT_LATE_24M_PCT,
		sum(case when -DAYS_INSTALMENT/30 <48 and AMT_INSTALMENT > 0 then AMT_PAYMENT_LATE end ) /
			sum(case when -DAYS_INSTALMENT/30 <48 and AMT_INSTALMENT > 0 then AMT_INSTALMENT end) AMT_PAYMENT_LATE_48M_PCT,
		sum(case when -DAYS_INSTALMENT/30 <72 and AMT_INSTALMENT > 0 then AMT_PAYMENT_LATE end ) /
			sum(case when -DAYS_INSTALMENT/30 <72 and AMT_INSTALMENT > 0 then AMT_INSTALMENT end) AMT_PAYMENT_LATE_72M_PCT

	into [HomeCredit].[dbo].installments_payments_data
from [HomeCredit].[dbo].[installments_payments_month] t
group by t.SK_ID_CURR;


select tr.SK_ID_CURR
		, tr.target
		,[INST_FLAG]
		,NUM_INSTALMENT_VERSION
      ,[DPD_MAX_INST]
      ,[DPD_MAX_3M_INST]
      ,[DPD_MAX_6M_INST]
      ,[DPD_MAX_12M_INST]
      ,[DPD_MAX_18M_INST]
      ,[DPD_MAX_24M_INST]
      ,[DPD_MAX_48M_INST]
      ,[DPD_MAX_72M_INST]
      ,[ACC_AGE_OLDEST_INST]
      ,[ACC_AGE_NEWEST_INST]
      ,[TIMES_DPD1_INST]
      ,[TIMES_DPD5_INST]
      ,[TIMES_DPD10_INST]
      ,[TIMES_DPD15_INST]
      ,[TIMES_DPD20_INST]
      ,TIMES_DPD1_24M_INST
      ,[ACC_DPD5_INST]
      ,[ACC_DPD10_INST]
      ,[ACC_DPD15_INST]
      ,[ACC_DPD20_INST]
      ,[ACC_NVER_DPD5_INST]
      ,[ACC_NVER_DPD10_INST]
      ,[ACC_NVER_DPD15_INST]
      ,[ACC_NVER_DPD20_INST]
      ,[ACC_DPD5_3M_INST]
      ,[ACC_DPD5_6M_INST]
      ,[ACC_DPD5_12M_INST]
      ,[ACC_DPD5_18M_INST]
      ,[ACC_DPD5_24M_INST]
      ,[ACC_DPD5_48M_INST]
      ,[ACC_DPD5_72M_INST]
      ,[ACC_DPD10_3M_INST]
      ,[ACC_DPD10_6M_INST]
      ,[ACC_DPD10_12M_INST]
      ,[ACC_DPD10_18M_INST]
      ,[ACC_DPD10_24M_INST]
      ,[ACC_DPD10_48M_INST]
      ,[ACC_DPD10_72M_INST]
      ,[ACC_DPD15_6M_INST]
      ,[ACC_DPD15_12M_INST]
      ,[ACC_DPD15_18M_INST]
      ,[ACC_DPD15_24M_INST]
      ,[ACC_DPD15_48M_INST]
      ,[ACC_DPD15_72M_INST]
      ,[ACC_CURR_DPD5_INST]
      ,[ACC_CURR_DPD10_INST]
      ,[ACC_CURR_DPD15_INST]
      ,[ACC_CURR_DPD20_INST]
      ,[ACC_CURR_DPD25_INST]
      ,[AMT_CURR_DPD5_INST]
      ,[AMT_CURR_DPD10_INST]
      ,[AMT_CURR_DPD15_INST]
      ,[AMT_CURR_DPD20_INST]
      ,[AMT_CURR_DPD25_INST]
into  [HomeCredit].[dbo].installments_payments_final
from [HomeCredit].[dbo].application_combined tr 
left join [HomeCredit].[dbo].installments_payments_data t on tr.SK_ID_CURR = t.SK_ID_CURR;

select x.* 
into HomeCredit.dbo.previous_application_data2
from	(
		select SK_ID_CURR
		,NAME_PAYMENT_TYPE
		,CODE_REJECT_REASON
		,NAME_TYPE_SUITE
		,NAME_CLIENT_TYPE
		,NAME_GOODS_CATEGORY
		,NAME_PORTFOLIO
		,NAME_PRODUCT_TYPE
		,CHANNEL_TYPE
		,NAME_SELLER_INDUSTRY
		,NAME_YIELD_GROUP
		,PRODUCT_COMBINATION
		,NFLAG_INSURED_ON_APPROVAL
		, ROW_NUMBER () OVER (partition by SK_ID_CURR order by DAYS_DECISION desc) row_cnt
		FROM HomeCredit.dbo.previous_application1) x
where row_cnt = 1;


  /****** Script for SelectTopNRows command from SSMS  ******/



SELECT SK_ID_CURR
      ,count(1) APP_CNT
      ,sum(case when CREDIT_ACTIVE = 'Closed' then 1 else 0 end) CLOSED_APP_CNT
      ,'G' + cast(sum(case when CREDIT_ACTIVE = 'Closed' then 1 else 0 end) as real)/count(1) CLOSED_APP_PCT
      ,min(-DAYS_CREDIT) DAYS_CREDIT_LAST
      ,max(-DAYS_CREDIT) DAYS_CREDIT_FIRST
      ,avg(-DAYS_CREDIT) DAYS_CREDIT_AVG
      
      ,max(CREDIT_DAY_OVERDUE) CREDIT_DAY_OVERDUE_MAX
      ,sum(case when CREDIT_DAY_OVERDUE > 1 then 1 else 0 end) OVERDUE1_CNT
      ,sum(case when CREDIT_DAY_OVERDUE > 3 then 1 else 0 end) OVERDUE3_CNT
      ,sum(case when CREDIT_DAY_OVERDUE > 5 then 1 else 0 end) OVERDUE5_CNT
      ,sum(case when CREDIT_DAY_OVERDUE > 10 then 1 else 0 end) OVERDUE10_CNT
      
      ,'G' + cast(sum(case when CREDIT_DAY_OVERDUE > 1 then 1 else 0 end) as real)/count(1) OVERDUE1_PCT
      
      ,sum(case when CREDIT_DAY_OVERDUE > 0 and -DAYS_CREDIT/30 <= 6 then 1 else 0 end) OVERDUE1_6M_CNT
      ,sum(case when CREDIT_DAY_OVERDUE > 0 and -DAYS_CREDIT/30 <= 12 then 1 else 0 end) OVERDUE1_12M_CNT
      ,sum(case when CREDIT_DAY_OVERDUE > 0 and -DAYS_CREDIT/30 <= 24 then 1 else 0 end) OVERDUE1_24M_CNT
      ,sum(case when CREDIT_DAY_OVERDUE > 0 and -DAYS_CREDIT/30 <= 36 then 1 else 0 end) OVERDUE1_36M_CNT
      
      ,max(DAYS_CREDIT_ENDDATE) DAYS_CREDIT_ENDDATE_MAX
      ,min(DAYS_CREDIT_ENDDATE) DAYS_CREDIT_ENDDATE_MIN
      ,avg(DAYS_CREDIT_ENDDATE) DAYS_CREDIT_ENDDATE_AVG
 
      ,MAX(case when CREDIT_ACTIVE = 'Closed' then DAYS_ENDDATE_FACT - DAYS_CREDIT_ENDDATE END) EARLY_TERM_DAYS_MAX
      ,min(case when CREDIT_ACTIVE = 'Closed' then DAYS_ENDDATE_FACT - DAYS_CREDIT_ENDDATE END) EARLY_TERM_DAYS_MIN
      
      ,sum(AMT_CREDIT_MAX_OVERDUE) AMT_CREDIT_MAX_OVERDUE_SUM
      ,case when SUM(AMT_CREDIT_SUM) > 0 
			then cast(sum(AMT_CREDIT_MAX_OVERDUE) as real)/SUM(AMT_CREDIT_SUM) end AMT_CREDIT_MAX_OVERDUE_PCT
      
      ,SUM(AMT_CREDIT_SUM) AMT_CREDIT_SUM_SUM
      ,max(AMT_CREDIT_SUM) AMT_CREDIT_SUM_MAX
      ,min(AMT_CREDIT_SUM) AMT_CREDIT_SUM_MIN
      ,avg(AMT_CREDIT_SUM) AMT_CREDIT_SUM_AVG
      
      ,SUM(case when CREDIT_TYPE = 'Credit card' then AMT_CREDIT_SUM else 0 end) AMT_CREDIT_CRC_SUM
      ,max(case when CREDIT_TYPE = 'Credit card' then AMT_CREDIT_SUM else 0 end) AMT_CREDIT_CRC_MAX
      ,min(case when CREDIT_TYPE = 'Credit card' then AMT_CREDIT_SUM else 0 end) AMT_CREDIT_CRC_MIN
      ,avg(case when CREDIT_TYPE = 'Credit card' then AMT_CREDIT_SUM else 0 end) AMT_CREDIT_CRC_AVG

      ,max(case when CREDIT_TYPE = 'Credit card' then AMT_CREDIT_SUM else 0 end) CNT_CREDIT_PROLONG_MAX
      
      ,case when SUM(AMT_CREDIT_SUM) > 0 
			then cast(sum(AMT_CREDIT_SUM_DEBT) as real)/SUM(AMT_CREDIT_SUM) end DEBT_REMAIN_PCT
      ,max(AMT_CREDIT_SUM_LIMIT) AMT_CREDIT_SUM_LIMIT
      ,max(case when (case when CREDIT_TYPE = 'Credit card' then AMT_CREDIT_SUM_LIMIT else 0 end ) > 0 
			then cast((case when CREDIT_TYPE = 'Credit card' then AMT_CREDIT_SUM else 0 end) as real)/
					 (case when CREDIT_TYPE = 'Credit card' then AMT_CREDIT_SUM_LIMIT else 0 end ) end ) AMT_CREDIT_USAGE_PCT
      ,sum(case when CREDIT_TYPE = 'Credit card' then 1 else 0 end) CREDIT_TYPE_CRC_CNT
      ,sum(case when CREDIT_TYPE = 'Consumer credit' then 1 else 0 end) CREDIT_TYPE_CONS_CNT
      ,sum(case when CREDIT_TYPE = 'Car loan' then 1 else 0 end) CREDIT_TYPE_CAR_CNT
      ,sum(case when CREDIT_TYPE = 'Mortgage' then 1 else 0 end) CREDIT_TYPE_MOR_CNT
      
      ,MIN(DAYS_CREDIT_UPDATE) DAYS_CREDIT_UPDATE_MIN
      ,MAX(DAYS_CREDIT_UPDATE) DAYS_CREDIT_UPDATE_MAX
      
      ,sum(AMT_ANNUITY) AMT_ANNUITY_SUM
      ,max(AMT_ANNUITY) AMT_ANNUITY_MAX
      ,min(AMT_ANNUITY) AMT_ANNUITY_MIN
      ,avg(AMT_ANNUITY) AMT_ANNUITY_AVG
into HomeCredit.dbo.bureau_data
  FROM HomeCredit.dbo.bureau1
  group by SK_ID_CURR;



SELECT t.SK_ID_CURR
	  ,t.TARGET
	  ,'G' + cast(NTILE(20) OVER(ORDER BY APP_CNT) as varchar(50)) as APP_CNT
      ,'G' + cast(NTILE(20) OVER(ORDER BY CLOSED_APP_CNT) as varchar(50)) as CLOSED_APP_CNT
      ,'G' + cast(NTILE(20) OVER(ORDER BY CLOSED_APP_PCT) as varchar(50)) as CLOSED_APP_PCT
      ,'G' + cast(NTILE(20) OVER(ORDER BY DAYS_CREDIT_LAST) as varchar(50)) as DAYS_CREDIT_LAST
      ,'G' + cast(NTILE(20) OVER(ORDER BY DAYS_CREDIT_FIRST) as varchar(50)) as DAYS_CREDIT_FIRST
      ,'G' + cast(NTILE(20) OVER(ORDER BY DAYS_CREDIT_AVG) as varchar(50)) as DAYS_CREDIT_AVG
      ,'G' + cast(NTILE(20) OVER(ORDER BY CREDIT_DAY_OVERDUE_MAX) as varchar(50)) as CREDIT_DAY_OVERDUE_MAX
      ,'G' + cast(OVERDUE1_CNT as varchar(50)) as OVERDUE1_CNT
      ,'G' + cast(OVERDUE3_CNT as varchar(50)) as OVERDUE3_CNT
      ,'G' + cast(OVERDUE5_CNT as varchar(50)) as OVERDUE5_CNT
      ,'G' + cast(OVERDUE10_CNT as varchar(50)) as OVERDUE10_CNT
      ,'G' + cast(OVERDUE1_PCT as varchar(50)) as OVERDUE1_PCT
      ,'G' + cast(OVERDUE1_6M_CNT as varchar(50)) as OVERDUE1_6M_CNT
      ,'G' + cast(OVERDUE1_12M_CNT as varchar(50)) as OVERDUE1_12M_CNT
      ,'G' + cast(OVERDUE1_24M_CNT as varchar(50)) as OVERDUE1_24M_CNT
      ,'G' + cast(OVERDUE1_36M_CNT as varchar(50)) as OVERDUE1_36M_CNT
      ,'G' + cast(NTILE(20) OVER(ORDER BY DAYS_CREDIT_ENDDATE_MAX) as varchar(50)) as DAYS_CREDIT_ENDDATE_MAX
      ,'G' + cast(NTILE(20) OVER(ORDER BY DAYS_CREDIT_ENDDATE_MIN) as varchar(50)) as DAYS_CREDIT_ENDDATE_MIN
      ,'G' + cast(NTILE(20) OVER(ORDER BY DAYS_CREDIT_ENDDATE_AVG) as varchar(50)) as DAYS_CREDIT_ENDDATE_AVG
      ,'G' + cast(NTILE(20) OVER(ORDER BY EARLY_TERM_DAYS_MAX) as varchar(50)) as EARLY_TERM_DAYS_MAX
      ,'G' + cast(NTILE(20) OVER(ORDER BY EARLY_TERM_DAYS_MIN) as varchar(50)) as EARLY_TERM_DAYS_MIN
      ,'G' + cast(NTILE(20) OVER(ORDER BY AMT_CREDIT_MAX_OVERDUE_SUM) as varchar(50)) as AMT_CREDIT_MAX_OVERDUE_SUM
      ,'G' + cast(NTILE(20) OVER(ORDER BY AMT_CREDIT_MAX_OVERDUE_PCT) as varchar(50)) as AMT_CREDIT_MAX_OVERDUE_PCT
      ,'G' + cast(NTILE(20) OVER(ORDER BY AMT_CREDIT_SUM_SUM) as varchar(50)) as AMT_CREDIT_SUM_SUM
      ,'G' + cast(NTILE(20) OVER(ORDER BY AMT_CREDIT_SUM_MAX) as varchar(50)) as AMT_CREDIT_SUM_MAX
      ,'G' + cast(NTILE(20) OVER(ORDER BY AMT_CREDIT_SUM_MIN) as varchar(50)) as AMT_CREDIT_SUM_MIN
      ,'G' + cast(NTILE(20) OVER(ORDER BY AMT_CREDIT_SUM_AVG) as varchar(50)) as AMT_CREDIT_SUM_AVG
      ,'G' + cast(NTILE(20) OVER(ORDER BY AMT_CREDIT_CRC_SUM) as varchar(50)) as AMT_CREDIT_CRC_SUM
      ,'G' + cast(NTILE(20) OVER(ORDER BY AMT_CREDIT_CRC_MAX) as varchar(50)) as AMT_CREDIT_CRC_MAX
      ,'G' + cast(NTILE(20) OVER(ORDER BY AMT_CREDIT_CRC_MIN) as varchar(50)) as AMT_CREDIT_CRC_MIN
      ,'G' + cast(NTILE(20) OVER(ORDER BY AMT_CREDIT_CRC_AVG) as varchar(50)) as AMT_CREDIT_CRC_AVG
      ,'G' + cast(NTILE(20) OVER(ORDER BY CNT_CREDIT_PROLONG_MAX) as varchar(50)) as CNT_CREDIT_PROLONG_MAX
      ,'G' + cast(NTILE(20) OVER(ORDER BY DEBT_REMAIN_PCT) as varchar(50)) as DEBT_REMAIN_PCT
      ,'G' + cast(NTILE(20) OVER(ORDER BY AMT_CREDIT_SUM_LIMIT) as varchar(50)) as AMT_CREDIT_SUM_LIMIT
      ,'G' + cast(NTILE(20) OVER(ORDER BY AMT_CREDIT_USAGE_PCT) as varchar(50)) as AMT_CREDIT_USAGE_PCT
      ,'G' + cast(CREDIT_TYPE_CRC_CNT as varchar(50)) CREDIT_TYPE_CRC_CNT
      ,'G' + cast(NTILE(20) OVER(ORDER BY CREDIT_TYPE_CONS_CNT) as varchar(50)) CREDIT_TYPE_CONS_CNT
      ,'G' + cast(CREDIT_TYPE_CAR_CNT as varchar(50)) CREDIT_TYPE_CAR_CNT
      ,'G' + cast(CREDIT_TYPE_MOR_CNT as varchar(50)) CREDIT_TYPE_MOR_CNT
      ,'G' + cast(NTILE(20) OVER(ORDER BY DAYS_CREDIT_UPDATE_MIN) as varchar(50)) as DAYS_CREDIT_UPDATE_MIN
      ,'G' + cast(NTILE(20) OVER(ORDER BY DAYS_CREDIT_UPDATE_MAX) as varchar(50)) as DAYS_CREDIT_UPDATE_MAX
      ,'G' + cast(NTILE(20) OVER(ORDER BY AMT_ANNUITY_SUM) as varchar(50)) as AMT_ANNUITY_SUM
      ,'G' + cast(NTILE(20) OVER(ORDER BY AMT_ANNUITY_MAX) as varchar(50)) as AMT_ANNUITY_MAX
      ,'G' + cast(NTILE(20) OVER(ORDER BY AMT_ANNUITY_MIN) as varchar(50)) as AMT_ANNUITY_MIN
      ,'G' + cast(NTILE(20) OVER(ORDER BY AMT_ANNUITY_AVG) as varchar(50)) as AMT_ANNUITY_AVG
  into HomeCredit.dbo.bureau_data_bin
  FROM HomeCredit.dbo.application_combined t
  left join HomeCredit.dbo.bureau_data b on b.SK_ID_CURR = t.SK_ID_CURR;

select x.* from
(
select tr.SK_ID_CURR, 
tr.TARGET
,'G'+cast(NTILE(20) OVER(ORDER BY EXT_SOURCE_3) as varchar(5)) as EXT_SOURCE_3
,'G'+cast(NTILE(20) OVER(ORDER BY EXT_SOURCE_2) as varchar(5)) as EXT_SOURCE_2
,'G'+cast(NTILE(20) OVER(ORDER BY EXT_SOURCE_1) as varchar(5)) as EXT_SOURCE_1
,'G'+cast(NTILE(20) OVER(ORDER BY DAYS_EMPLOYED) as varchar(5)) as DAYS_EMPLOYED
,'G'+cast(NTILE(20) OVER(ORDER BY DAYS_BIRTH) as varchar(5)) as DAYS_BIRTH
,ORGANIZATION_TYPE
,'G'+cast(NTILE(20) OVER(ORDER BY AMT_GOODS_PRICE) as varchar(5)) as AMT_GOODS_PRICE
,'G'+cast(NTILE(20) OVER(ORDER BY REGION_RATING_CLIENT) as varchar(5)) as REGION_RATING_CLIENT
,'G'+cast(NTILE(20) OVER(ORDER BY AMT_CREDIT) as varchar(5)) as AMT_CREDIT
,NAME_EDUCATION_TYPE
,'G'+cast(NTILE(20) OVER(ORDER BY REGION_POPULATION_RELATIVE) as varchar(5)) as REGION_POPULATION_RELATIVE
,'G'+cast(NTILE(20) OVER(ORDER BY DAYS_LAST_PHONE_CHANGE) as varchar(5)) as DAYS_LAST_PHONE_CHANGE
,'G'+cast(NTILE(20) OVER(ORDER BY AMT_ANNUITY) as varchar(5)) as AMT_ANNUITY
,'G'+cast(NTILE(20) OVER(ORDER BY FLOORSMAX_MEDI) as varchar(5)) as FLOORSMAX_MEDI
,NAME_FAMILY_STATUS
,DOCUMENT_GROUP
,FLAG_OWN_CAR
,WEEKDAY_APPR_PROCESS_START
,'G'+cast(NTILE(20) OVER(ORDER BY DEF_30_CNT_SOCIAL_CIRCLE) as varchar(5)) as DEF_30_CNT_SOCIAL_CIRCLE
,OCCUPATION_TYPE
,AMT_DOWN_PAYMENT_MAX


,'G'+cast(NTILE(20) OVER(ORDER BY AMT_ANNUITY_APR_SUM) as varchar(5)) as AMT_ANNUITY_APR_SUM
,NAME_YIELD_GROUP
,'G' + cast(NTILE(20) OVER(ORDER BY CNT_PAYMENT_MAX) as varchar(5)) as CNT_PAYMENT_MAX
,'G' + cast(NTILE(20) OVER(ORDER BY ACC_AGE_OLDEST_INST) as varchar(5)) as ACC_AGE_OLDEST_INST
,'G' + cast(NTILE(10) OVER(ORDER BY DPD_MAX_24M_INST) as varchar(5)) as DPD_MAX_24M_INST
,'G' + cast(NTILE(10) OVER(ORDER BY TIMES_DPD5_INST) as varchar(5)) as TIMES_DPD5_INST

,bb.DAYS_CREDIT_AVG
,bb.DEBT_REMAIN_PCT
,bb.CLOSED_APP_PCT
,bb.DAYS_CREDIT_LAST
,bb.AMT_CREDIT_MAX_OVERDUE_PCT
,bb.DAYS_CREDIT_ENDDATE_MAX
,bb.CNT_CREDIT_PROLONG_MAX
,bb.AMT_CREDIT_SUM_MAX
,bb.CREDIT_TYPE_CRC_CNT

from HomeCredit.dbo.application_combined tr
left join HomeCredit.dbo.previous_application_data2 d on tr.SK_ID_CURR = d.SK_ID_CURR
left join HomeCredit.dbo.previous_application_bin b on tr.SK_ID_CURR = b.SK_ID_CURR
left join HomeCredit.dbo.installments_payments_final ip on ip.SK_ID_CURR = tr.SK_ID_CURR
left join HomeCredit.dbo.bureau_data_bin bb on bb.SK_ID_CURR = tr.SK_ID_CURR
) x
where x.TARGET is null
;
