---
title: "KaggleCompetition"
output: html_document
---


import all data set
```{r}
application_train <- read.csv("C:/Users/Tuan/Desktop/home-credit-default-risk/application_train.csv",header = TRUE)
application_test <- read.csv("C:/Users/Tuan/Desktop/home-credit-default-risk/application_test.csv",header = TRUE)

```

create train and validation sample:
1. Check randomness of new split
2. Create train and validation sample 80:20

```{r}
application_train$RANDOM_NB <- sample(1:100, nrow(application_train), replace = T)
hist(application_train$RANDOM_NB, border="blue", col="white")
```


```{r}
application_train1 <- application_train[which(application_train$RANDOM_NB > 20),]
application_val <- application_train[which(application_train$RANDOM_NB <= 20),]
message("Number of train sample: ", nrow(application_train1), " accounting for "
        ,floor(nrow(application_train1)/nrow(application_train)*100), "%")
message("Number of valication sample: ",nrow(application_val))
```

Import important library
```{r}
library("rpart")
library("rpart.plot")
library("MLmetrics")
library("caret")
```

Fit a decision tree and measure performance

```{r}
#Train a decision tree
tree <- rpart(formula = application_train1$TARGET~.
                        , data = application_train1
                        , control = rpart.control(cp = 0.00001
                                                  , minbucket = 2200
                                                  , maxdepth = 9
                                                  ))


#Predict on train and validation set
application_train1$PREDICT <- predict(tree, application_train1, type = "matrix")
application_val$PREDICT <- predict(tree, application_val, type = "matrix")

#Print performance
message("Gini on Train data ",Gini(y_pred = application_train1$PREDICT, y_true = application_train1$TARGET))
message("Gini on Validation data ",Gini(y_pred = application_val$PREDICT, y_true = application_val$TARGET))

```



Implement tree with Caret using 10-fold crossvalidate

```{r}
application_train$TARGET = as.numeric(application_train$TARGET)
dtree_fit <- train(form = TARGET~.
                   , data = application_train
                   , method = "rpart"
                   , trControl = trainControl(method = "cv", number = 10)
                   , tuneLength = 10
                   , parms=list(split='information')
                   , preProcess = c("center", "scale")
                   , na.action = na.pass)

application_train1$PREDICT <- predict(dtree_fit,application_train1, type = "prob")[2]
application_val$PREDICT <- predict(dtree_fit,application_val, type = "prob", preProcess = c("center", "scale"))[2]

#Print performance
message("Gini on Train data ",Gini(y_pred = application_train1$PREDICT, y_true = application_train1$TARGET))
message("Gini on Validation data ",Gini(y_pred = application_val$PREDICT, y_true = application_val$TARGET))

```
calculate missing value
```{r}
library(data.table)
#Function to change index to column
index_to_col <- function(data, Column_Name){
          data <- cbind(newColName = rownames(data), data)
          rownames(data) <- 1:nrow(data)
          colnames(data)[1] <- Column_Name
          return (data)
        }

mv <- as.data.frame(apply(tr, 2, function(col)sum(is.na(col))/length(col)))
colnames(mv)[1] <- "missing_values"
mv <- index_to_col(mv,'Column')
mv <- setDT(mv)[order (missing_values, decreasing = TRUE)]

ggplot (mv[1:40,], aes (reorder(Column, missing_values), missing_values)) + geom_bar (position = position_dodge(), stat = "identity") + coord_flip () + xlab('Columns') + ylab('Missing Value %')
```