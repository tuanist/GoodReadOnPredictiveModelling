
#QUINTILE BIN
selected_feature_bin = pd.DataFrame()
total = selected_feature.shape[0]
#assign distribution of a bin
bin_dist = 0.05
#iterate through columns
for column in selected_feature.columns:
    if selected_feature[column].dtype == float or selected_feature[column].dtype == int:
        print(column)
        #count null 
        null_cnt = selected_feature[column].isna().sum()
        #calculate number of bin. By default, quintile does not apply on NaN value. We have to deduct null.
        bin_cnt = ((total - null_cnt)/(total*bin_dist)).astype(int)
        if bin_cnt <= 0:
            print('Column '+ column +' ignored because of too many null')
            continue
        elif selected_feature[column].unique().size < 20:
            print('Column '+ column +' ignored because of not enough values')
            selected_feature_bin[column+'_BIN'] = selected_feature[column]
            continue
        #create new BIN column
        selected_feature_bin[column+'_BIN'] = pd.qcut(selected_feature[column],bin_cnt,precision=2,duplicates='drop').astype(str)


#INFORMATION VALUE
iv = pd.DataFrame(columns = ['VARIABLE','IV'])
#iterate through columns
for column in selected_feature_bin.columns:
    data = selected_feature_bin.groupby([column]).agg({'TARGET_BIN': ['sum','size']})
    data.columns = ['BAD','TOTAL']
    data['BAD_DIST'] = data['BAD']/data.BAD.sum()
    data['GOOD_DIST'] = (data['TOTAL'] - data['BAD'])/(data.TOTAL.sum() - data.BAD.sum() )
    data['WOE'] = np.log(data['GOOD_DIST']/data['BAD_DIST'])
    data['IV'] = data['WOE']*(data['GOOD_DIST'] - data['BAD_DIST'])
    inv = data['IV'].sum()
    iv = iv.append(pd.DataFrame({'VARIABLE': [column], 'IV': [inv]}))
iv = iv.sort_values(by='IV', ascending=False)
iv